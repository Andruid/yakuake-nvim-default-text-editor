#!/bin/sh
INITIAL_ID=`qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.activeSessionId`
function addSession {
    SESSION_ID=`qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.addSession`
    qdbus org.kde.yakuake /yakuake/tabs setTabTitle $SESSION_ID "$1"
    qdbus org.kde.yakuake /yakuake/window org.kde.yakuake.toggleWindowState
}

file="$@"
addSession "${file}" "nvim ${file}"
